﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Article.aspx.cs" Inherits="FinalProject_N00669744a.Article1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2 id="articleTitle" runat="server">Article Title: </h2>
    <div id="noArticle" runat="server"></div>

   <asp:SqlDataSource runat="server" id="articleSelect" ConnectionString="<%$ ConnectionStrings:article_con %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="deleteArticle" ConnectionString="<%$ ConnectionStrings:article_con %>">
    </asp:SqlDataSource>

    <div id="articleActions" class="pull-right mr-5">
        <a class="editLink" href="editArticle.aspx?articleID=<%Response.Write(this.articleid);%>">Edit Article</a>
        <asp:Button runat="server" id="removeArticle" OnClick="Delete_Article" OnClientClick="if(!confirm('Are you sure you want to delete this article?')) return false;" Text="Delete Article" />
    </div>

    <div id="previewArea" runat="server"></div>
</asp:Content>
