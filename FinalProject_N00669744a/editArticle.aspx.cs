﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject_N00669744a
{
    public partial class EditArticle : System.Web.UI.Page
    {
        public int articleid
        {
            get { return Convert.ToInt32(Request.QueryString["articleID"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        /*Code referenced from Christine Bittle*/
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView articlerow = PullArticleContent(articleid);
            if (articlerow == null)
            {
                articleName.InnerHtml = "Article Not Found";
                editArticle.Visible = false;
                return;
            }
            else
            {
                editArticle.Visible = true;
                articleName.InnerHtml += articlerow["articleTitle"].ToString();
                articleTitle.Text = articlerow["articleTitle"].ToString();
                articleAuthor.Text = articlerow["articleAuthor"].ToString();
                articleContent.Text = articlerow["articleContent"].ToString();
                articlePublished.InnerHtml = " "+articlerow["articleDate"].ToString().Substring(0, 10);
            }
        }
        protected void Edit_Article(object sender, EventArgs e)
        {
            string articletitle = articleTitle.Text.ToString();
            string articleauthor = articleAuthor.Text.ToString();
            string articlecontent = articleContent.Text.ToString();
            string articleEditDate = DateTime.Today.ToString();
            string datequery = "convert(varchar, '" + articleEditDate + "')";

            string editQuery = "Update Articles set articleTitle='" + articletitle + "', articleAuthor='" +
                articleauthor + "', articleContent ='" + articlecontent + "', articleChangeDate="+datequery;
            string whereClause = " where articleID=" + articleid;
            string fullQuery = editQuery + whereClause;

            edit_article.UpdateCommand = fullQuery;
            edit_article.Update();
            Response.Redirect("Article.aspx?articleid="+articleid);
        }

        protected DataRowView PullArticleContent(int artid)
        {
            string query = "select * from articles where articleID=" + articleid.ToString();
            articleSelect.SelectCommand = query;
            DataView articleview = (DataView)articleSelect.Select(DataSourceSelectArguments.Empty);
            if (articleview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView articlerowview = articleview[0]; 
            return articlerowview;
        }

    }
}