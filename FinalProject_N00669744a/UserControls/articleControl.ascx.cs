﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace FinalProject_N00669744a.UserControls
{
    public partial class articleControl : System.Web.UI.UserControl
    {
        private string query = "select articleid, articletitle from articles";
        

        protected void Page_Load(object sender, EventArgs e)
        {
            articles_update.SelectCommand = query;
            Article_Bind(articles_update);
        }

        void Article_Bind(SqlDataSource src)
        {
            DataView artview = (DataView)src.Select(DataSourceSelectArguments.Empty);


            
            

        
            foreach (DataRowView article_row in artview)
            {
                HtmlGenericControl article_li = new HtmlGenericControl("li");
                article_li.Attributes.Add("class","article_link");
                article_list.Controls.Add(article_li);
                HtmlGenericControl article_link = new HtmlGenericControl("a");
                article_link.Attributes.Add("href", "Article.aspx?articleID=" + article_row["articleid"]);
                article_link.InnerText = article_row["articletitle"].ToString();
                article_li.Controls.Add(article_link);
            }


        }

    }
}