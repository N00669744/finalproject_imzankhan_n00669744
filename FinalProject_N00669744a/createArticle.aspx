﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateArticle.aspx.cs" Inherits="FinalProject_N00669744a.createArticle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Create Article</h1>

      <asp:SqlDataSource runat="server" id="insertArticle"
        ConnectionString="<%$ ConnectionStrings:article_con %>">

    </asp:SqlDataSource>
        <div id="CreateArticle" >
            <div class="form-group">
                <label runat="server" for="articleTitle" class="col-form-label">Article Title: </label>
                 <asp:TextBox runat="server" class="form-control" ID="articleTitle" placeholder="Enter your Article title..."></asp:TextBox>
                 <asp:RequiredFieldValidator class="validation col-4" runat="server" ControlToValidate="articleTitle" ErrorMessage="Please enter an Article Title"></asp:RequiredFieldValidator>
           </div>
            <div class="form-group">
                <label runat="server" for="articleAuthor" class="col-form-label">Author: </label>
                <asp:TextBox runat="server" class="form-control" ID="articleAuthor" placeholder="Enter Article's Author..."></asp:TextBox>
                <asp:RequiredFieldValidator class="validation" runat="server" ControlToValidate="articleAuthor" ErrorMessage="Please enter the Author's Name"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
                <label runat="server" for="articleContent" class="col-form-label">Article: </label>
                <asp:TextBox runat="server" class="form-control" TextMode="MultiLine" Columns="100" Rows="5" ID="articleContent" placeholder="Enter your article here..."></asp:TextBox>
                <asp:RequiredFieldValidator class="validation" runat="server" ControlToValidate="articleContent" ErrorMessage="Please enter your Article here"></asp:RequiredFieldValidator>
            </div>
            <div id="submitOption" class="pull-right">
                <asp:Button runat="server" ID="previewBtn" Text="Preview" OnClick="PreviewArticle"/>
                <asp:Button runat="server" ID="submitBtn" Text="Submit" OnClick="AddArticle"/>
            </div>
            <div id="previewArea" class="col-12" runat="server"></div>
        </div> <!-- end of createArticle -->
</asp:Content>
