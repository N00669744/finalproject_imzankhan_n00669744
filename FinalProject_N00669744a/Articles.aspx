﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Articles.aspx.cs" Inherits="FinalProject_N00669744a.Articles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

     <h1>Articles</h1>
    <div id="articles-wrapper">
        <div>
            <label>Search Articles: </label>
            <asp:TextBox runat="server" ID="searchbox"> </asp:TextBox>
            <asp:Button runat="server" Text="Search" OnClick="SearchTitle"/>
        </div>
        <a href="createArticle.aspx" class="pull-right">&#43; Add Article</a>

        <asp:SqlDataSource runat="server" id="articlesSelect" ConnectionString="<%$ ConnectionStrings:article_con %>">
        </asp:SqlDataSource>

        <asp:DataGrid id="articlesListing" runat="server"></asp:DataGrid>
    </div>
</asp:Content>
