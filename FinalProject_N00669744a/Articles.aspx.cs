﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject_N00669744a
{
    public partial class Articles : System.Web.UI.Page
    {
        private string articleQuery = "select articleID, articleTitle as 'Article Title', articleAuthor as 'Author Name',"
            + "convert(varchar,articleDate,101) as 'Publish Date', convert(varchar,articleChangeDate,101) as 'Last Modified' from Articles";

        
        protected void Page_Load(object sender, EventArgs e)
        {
            articlesSelect.SelectCommand = articleQuery;
            articlesListing.DataSource = Link_ID(articlesSelect);
            articlesListing.DataBind();
        }

        protected void SearchTitle(object sender, EventArgs e)
        {
            string titleName = searchbox.Text.ToLower();
            string whereQuery = " where LOWER(articleTitle) like '%" + titleName + "%'";

            articlesSelect.SelectCommand = articleQuery+whereQuery;
            articlesListing.DataSource = Link_ID(articlesSelect);
            articlesListing.DataBind();
        } 


        protected DataView Link_ID(SqlDataSource src)
        { /*Code referenced from Christine Bittle, crud-basics example*/
            /* code used to remove the articleid from the listing and replace with its corresponding
             * articletitle */
            DataTable arttbl;
            DataView artview;

            arttbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            foreach (DataRow row in arttbl.Rows)
            {
                row["Article Title"] =
                    "<a href=\"Article.aspx?articleid="
                    + row["articleid"]
                    + "\">"
                    + row["Article Title"]
                    + "</a>";
            }
            arttbl.Columns.Remove("articleid");
            artview = arttbl.DefaultView;

            return artview;
        }
    }
}