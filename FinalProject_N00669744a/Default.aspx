﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FinalProject_N00669744a._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Welcome to the Article Database</h2>
    <div>
        <ul>
          <li><a runat="server" href="~/Articles">Manage Articles</a></li>
          <li><a runat="server" href="~/createArticle">Create New Article</a></li>
        </ul>
    </div>
</asp:Content>
