﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Globalization;
using System.Web.UI.WebControls;

namespace FinalProject_N00669744a
{
    public partial class createArticle : System.Web.UI.Page
    {
     
        private string addquery = "insert into ARTICLES(articleTitle,articleAuthor,articleContent,articleDate,articleChangeDate) " + 
            "VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AddArticle(object sender, EventArgs e)
        {
            string articletitle = articleTitle.Text.ToString();
            string articleauthor = articleAuthor.Text.ToString();
            string articlecontent = articleContent.Text.ToString();  
            string articledate = DateTime.Today.ToString();
            string datequery = "convert(varchar, '" + articledate + "')";

            addquery += "('" +articletitle + "','" + articleauthor + "','" + articlecontent + "'," + datequery+ "," + datequery + ")";

            insertArticle.InsertCommand = addquery;
            insertArticle.Insert();
            Response.Redirect("Articles.aspx");

        }

        protected void PreviewArticle(object sender, EventArgs e)
        {
            string articletitle = articleTitle.Text.ToString();
            string articleauthor = articleAuthor.Text.ToString();
            string articlecontent = articleContent.Text.ToString();
            string articledate = DateTime.Now.ToString().Substring(0, 10);

            Article newarticleinfo = new Article(articletitle, articleauthor, articlecontent, articledate, articledate);
            previewArea.InnerHtml = newarticleinfo.PreviewDetails();
        }
    }
}