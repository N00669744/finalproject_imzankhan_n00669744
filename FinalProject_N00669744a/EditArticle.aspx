﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="editArticle.aspx.cs" Inherits="FinalProject_N00669744a.EditArticle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2 runat="server" id="article_name">Edit Article: </h2>
    <asp:SqlDataSource runat="server" id="edit_article" ConnectionString="<%$ ConnectionStrings:article_con %>">

    </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="articleSelect" ConnectionString="<%$ ConnectionStrings:article_con %>">
    </asp:SqlDataSource>

    <h3 id="articleName" runat="server"></h3>
    <div id="editArticle" runat="server">
           <div class="form-group row">
                <label runat="server" for="articleTitle" class="col-2 col-form-label">Article Title: </label>
                <div class="col-10">
                    <asp:TextBox runat="server" class="col-6 form-control" ID="articleTitle" placeholder="Enter your Article title..."></asp:TextBox>
                 </div>
                 <asp:RequiredFieldValidator class="validation col-4" runat="server" ControlToValidate="articleTitle" ErrorMessage="Please enter an Article Title"></asp:RequiredFieldValidator>
           </div>
            <div class="form-group row">
                <label runat="server" for="articleAuthor" class="col-2 col-form-label">Author: </label>
                <asp:TextBox runat="server" class="col-10 form-control" ID="articleAuthor" placeholder="Enter Article's Author..."></asp:TextBox>
                <asp:RequiredFieldValidator class="validation" runat="server" ControlToValidate="articleAuthor" ErrorMessage="Please enter the Author's Name"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group row">
                <label runat="server" for="articleContent" class="col-2 col-form-label">Article: </label>
                <asp:TextBox runat="server" class="form-control" TextMode="MultiLine" Columns="100" Rows="5" ID="articleContent" placeholder="Enter your article here..."></asp:TextBox>
                <asp:RequiredFieldValidator class="validation" runat="server" ControlToValidate="articleContent" ErrorMessage="Please enter your Article here"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label runat="server" for="articleContent">Article Publish Date: </label><span runat="server" id="articlePublished"></span>
            </div>
            <div id="submitOption">
                <asp:Button runat="server" ID="submitBtn" class="pull-right" Text="Submit" OnClick="Edit_Article"/>
            </div>
            <div id="previewArea" runat="server"></div>
        </div> <!-- end of editArticle -->
</asp:Content>
