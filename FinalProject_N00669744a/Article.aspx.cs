﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject_N00669744a
{
    public partial class Article1 : System.Web.UI.Page
    {
        public string articleid
        {
            get { return Request.QueryString["articleID"]; }
        }

        private string articleInfo_query = "select articleID, articleContent as 'Article Content', articleTitle as 'Article Title', articleAuthor as 'Article Author',"
            + "convert(varchar,articleDate,101) as 'Publish Date', convert(varchar,articleChangeDate,101) as 'Last Modified' from Articles";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (articleid == null || articleid == "")
            {
                articleTitle.InnerHtml += " N/A"; 
                noArticle.InnerHtml = "No Article Found...";
                removeArticle.Visible = false;
            }
            else
            {
                removeArticle.Visible = true;
                string whereClause = " where articleid=" + articleid;
                string fullQuery = articleInfo_query + whereClause;
                articleSelect.SelectCommand = fullQuery;
          
                /* Code referenced from Christine Bittle */
                DataView articleView =(DataView)articleSelect.Select(DataSourceSelectArguments.Empty);
                DataRowView oneArticle = articleView[0]; 

                string articletitle = articleView[0]["Article Title"].ToString();
                string articleauthor = articleView[0]["Article Author"].ToString();
                string articlecontent = articleView[0]["Article Content"].ToString();
                string articledate = articleView[0]["Publish Date"].ToString();
                string editdate = articleView[0]["Last Modified"].ToString();

                Article newarticleinfo = new Article(articletitle, articleauthor, articlecontent, articledate, editdate);
                previewArea.InnerHtml = newarticleinfo.PreviewDetails();

                articleTitle.InnerHtml += articletitle;
            }
        }

        protected DataView Link_ID(SqlDataSource src)
        { /*Code referenced from Christine Bittle, crud-basics example*/
            /* code used to remove the articleid from the listing and replace with its corresponding
             * articletitle */
            DataTable arttbl;
            DataView artview;

            arttbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            foreach (DataRow row in arttbl.Rows)
            {
                row["Article Title"] =
                    "<a href=\"Article.aspx?articleid="
                    + row["articleid"]
                    + "\">"
                    + row["Article Title"]
                    + "</a>";
            }
            arttbl.Columns.Remove("articleid");  
            artview = arttbl.DefaultView;
            return artview;
        }

        protected void Delete_Article(object sender,EventArgs e)
        {
            string delete_query = "delete from articles where articleID="+ articleid;
            deleteArticle.DeleteCommand = delete_query;
            deleteArticle.Delete();
            Response.Redirect("Articles.aspx");
        }
    }
}