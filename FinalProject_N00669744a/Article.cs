﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace FinalProject_N00669744a
{
    public class Article
    {
        public string articlename;
        public string articleauthor;
        public string article;
        public string published;
        public string editdate;

        public Article(string name, string auth, string art, string pub, string edit)
        {
            articlename = name;
            articleauthor = auth;
            article = art;
            published = pub;
            editdate = edit;
        }

        public string PreviewDetails()
        {
            string previewArea = "";
            string dateFormat = published;

            previewArea += "<div id='previewArticle' class='col-lg-12'> <h2 id='previewTitle'>" + articlename + "</h2>";
            previewArea += "<p id='previewAuthor'><em>" + articleauthor + "</em> | <em>" + dateFormat+ "</em></p>";
            previewArea += "<p id='previewContent'>"+article+"</p>";
            previewArea += "<div>Last Modified:" + editdate + "</div> </div>";

            return previewArea;
        }

        
    }
}